﻿using System.Collections.Generic;

namespace Cw.Models
{
    public class Place
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string MainPhotoPath { get; set; }
        public List<Photos> Photos { get; set; }
        public List<Reviews> Reviews { get; set; }
        public List<Rate> Rates { get; set; }

    }
}