﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Cw.Models
{
    public class PlacesViewModel
    {
        public string Description { get; set; }
        public string Name { get; set; }
        public string MainPhotoPath { get; set; }
        public IFormFile Photo { get; set; }
        public List<Place> Places { get; set; }
    }
}
