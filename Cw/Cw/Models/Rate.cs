﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cw.Models
{
    public class Rate
    {
        public string Id { get; set; }
        public string PlaceId { get; set; }
        public Place Place { get; set; }
        public double Rating { get; set; } = 0.0;
    }
}
