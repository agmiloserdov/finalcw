﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cw.Models
{
    public class Photos
    {
        public string Id { get; set; }
        public string PlaceId { get; set; }
        public Place Place { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public string FilePath { get; set; }
    }
}
