﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Cw.Data;
using Microsoft.AspNetCore.Mvc;
using Cw.Models;
using Microsoft.EntityFrameworkCore;

namespace Cw.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _db;
        public HomeController(ApplicationDbContext db)
        {
            _db = db;
        }

        
        public IActionResult Index()
        {
            PlacesViewModel model = new PlacesViewModel();
            try
            {
                model.Places = _db.Places.OrderBy(p => p.Name).ToList();
                foreach (var place in model.Places)
                {
                    place.Photos = _db.Photos.Where(p => p.PlaceId == place.Id).ToList();
                    place.Reviews = _db.Reviews.Where(rw => rw.PlaceId == place.Id).ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return View(model);
            }
            return View(model);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
