﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Cw.Data;
using Cw.Models;
using Cw.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Cw.Controllers
{
    public class ReviewController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly IHostingEnvironment _environment;
        private readonly UploadFileService _fileUploadService;

        public ReviewController(ApplicationDbContext db, IHostingEnvironment environment, UploadFileService fileUploadService)
        {
            _db = db;
            _environment = environment;
            _fileUploadService = fileUploadService;
        }

        public IActionResult AddReview(string Review, double Rate, string UserId, string PlaceId)
        {
            var user = _db.Users.FirstOrDefault(u => u.Id == UserId);
            Reviews review = new Reviews
            {
                Review = Review,
                Rate = Rate,
                PlaceId = PlaceId,
                UserId = UserId,
                UserName = user.UserName,
                ReviewDate = DateTime.Now
            };
            _db.Reviews.Add(review);
            _db.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        public IActionResult UploadPlacePhoto(IFormFile Photo, string UserId, string PlaceId)
        {
            string path = Path.Combine(_environment.WebRootPath, $"images\\");
            string photoPath = $"/images/{Photo.FileName}";
            _fileUploadService.Upload(path, Photo.FileName, Photo);
            Photos photo = new Photos
            {
                FilePath = photoPath,
                PlaceId = PlaceId,
                UserId = UserId
            };
            _db.Photos.Add(photo);
            _db.SaveChanges();
            return RedirectToAction("Index", "Home");
        }
    }
}
