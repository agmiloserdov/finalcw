﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Cw.Data;
using Cw.Models;
using Cw.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace Cw.Controllers
{
    public class PlaceController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly IHostingEnvironment _environment;
        private readonly UploadFileService _fileUploadService;

        public PlaceController(ApplicationDbContext db, IHostingEnvironment environment, UploadFileService fileUploadService)
        {
            _db = db;
            _environment = environment;
            _fileUploadService = fileUploadService;
        }

        [Authorize]
        [HttpPost]
        public IActionResult AddPlace(PlacesViewModel model)
        {
            
            if (ModelState.IsValid)
            {
                try
                {
                    string path = Path.Combine(_environment.WebRootPath,$"images\\");
                    string photoPath = $"/images/{model.Photo.FileName}";
                    _fileUploadService.Upload(path, model.Photo.FileName, model.Photo);
                    Place place = new Place
                    {
                        Name = model.Name,
                        MainPhotoPath = photoPath,
                        Description = model.Description
                    };
                    
                    _db.Places.Add(place);
                    _db.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return RedirectToAction("Index", "Home");
                }
            }
            return RedirectToAction("Index", "Home");
        }

        public IActionResult AddPlace()
        {
            PlacesViewModel model = new PlacesViewModel();
            return View(model);
        }

        public IActionResult PlaceDetails(string placeId)
        {
            Place model = _db.Places.FirstOrDefault(p => p.Id == placeId);
            model.Photos = _db.Photos.Where(p => p.PlaceId == model.Id).ToList();
            model.Rates = _db.Rates.Where(r => r.PlaceId == model.Id).ToList();
            model.Reviews = _db.Reviews.Where(rw => rw.PlaceId == model.Id).ToList();
            return View(model);
        }

        public IActionResult FindPlaceByName(string Name)
        {
            List<Place> places = _db.Places.Where(p => p.Name.Contains(Name)).ToList();
            foreach (var place in places)
            {
                place.Photos = _db.Photos.Where(p => p.PlaceId == place.Id).ToList();
                place.Reviews = _db.Reviews.Where(rw => rw.PlaceId == place.Id).ToList();
            }
            return View(places);
        }
    }
}
